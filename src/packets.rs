use heapless::Vec;
use crate::packets::Packets::Ping;


// The first byte identifies the packet.
//#[repr(u8)]
pub enum Packets
{
    Ping = 2
}

impl Into<heapless::Vec<u8, 255>> for Packets
{
    fn into(self) -> Vec<u8, 255>
    {
        let mut v: heapless::Vec<u8, 255> = heapless::Vec::new();

        match self
        {
            Packets::Ping => {
                v.push(Ping as u8);

                v
            }
        }
    }
}
