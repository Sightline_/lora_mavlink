
// https://github.com/mavlink/rust-mavlink/blob/master/examples/embedded/src/main.rs

#![no_std]
#![no_main]


mod packets;
use feather_m0 as hal;
use panic_semihosting as _;
//use sx127x_lora::LoRa;
//use sx127x_lora::RadioMode;
//use sx127x_lora::RadioMode::*;
//use radio_sx127x;
use feather_m0::delay::Delay;
use feather_m0::clock::GenericClockController;
use feather_m0::time::MegaHertz;
use feather_m0::gpio::v2::*;
use crate::hal::sercom::*;
use feather_m0::eic::pin::ExtInt9;
use no_panic::no_panic;
use feather_m0::prelude::EicPin;
use feather_m0::eic::pin::Sense;
use feather_m0::eic::EIC;
use embedded_hal::blocking::spi::{Write, Transfer};
use feather_m0::time::U32Ext;
use feather_m0::prelude::_atsamd_hal_embedded_hal_digital_v2_OutputPin;
use cortex_m::asm::delay as cortex_delay;
use cortex_m_semihosting::hprintln;
use feather_m0::usb::usb_device::prelude::*;
use hal::pac::interrupt;
use feather_m0::usb::UsbBus;
use feather_m0::prelude::*;
use radio_sx127x;
use radio_sx127x::prelude::*;
use radio::{Transmit, Receive};
//use bitflags::bitflags;

use hal::prelude::*;

//use hal::usbd_serial::{SerialPort, USB_CLASS_CDC};

use embedded_hal::blocking::delay::{DelayMs, DelayUs};
use embedded_hal::digital::v2::{OutputPin, InputPin};
use embedded_hal_compat::eh1_0::blocking::delay::DelayMs as _;
use usbd_serial::SerialPort;

use nb;
use nb::block;
use feather_m0::gpio::PfC;
use heapless::Vec;
use bbqueue::{BBBuffer, ConstBBBuffer};
use crate::packets::Packets::Ping;
use radio_sx127x::prelude::*;
use radio_sx127x::device::lora::*;
use radio_sx127x::prelude::*;
use radio_sx127x::device::lora::*;
use embedded_hal_compat::{IntoCompat, Compat};
use core::convert::Infallible;
use radio_sx127x::{device::lora::{
    Bandwidth, CodingRate, FrequencyHopping, LoRaChannel, LoRaConfig, PayloadCrc,
    PayloadLength, SpreadingFactor,
}, device::{Channel, Modem, PaConfig, PaSelect}, prelude::*, Sx127xSpi};
use feather_m0::gpio;

use hal::rtc::Rtc;
use feather_m0::rtc::ClockMode;
use radio_sx127x::device::{State, regs};

//use core::fmt::Debug;*/


//use mavlink;

const FREQUENCY:  u32 = 915_000_000;
const TOTAL_NODES: u8 = 2;
const SELF_ADDRESS: u8 = 0;

/*pub struct Network
{
    pub nodes: Vec<Node, net>
}*/

const CONFIG_CH: LoRaChannel = LoRaChannel {
    freq: FREQUENCY, // frequency in hertz
    bw: Bandwidth::Bw500kHz,
    sf: SpreadingFactor::Sf7,
    cr: CodingRate::Cr4_8,
};

const CONFIG_LORA: LoRaConfig = LoRaConfig {
    preamble_len: 0x8,
    symbol_timeout: 0x64,
    payload_len: PayloadLength::Variable,
    payload_crc: PayloadCrc::Enabled,
    frequency_hop: FrequencyHopping::Disabled,
    invert_iq: false,
};

const CONFIG_PA: PaConfig = PaConfig {
    output: PaSelect::Boost,
    power: 14,
};

//let CONFIG_RADIO = Config::default() ;

const CONFIG_RADIO: radio_sx127x::device::Config = radio_sx127x::device::Config {
    modem: Modem::LoRa(CONFIG_LORA),
    channel: Channel::LoRa(CONFIG_CH),
    pa_config: CONFIG_PA,
    xtal_freq: 32000000, // CHECK
    timeout_ms: 100,
};

type SPI    = SPIMaster4<hal::sercom::Sercom4Pad0<gpio::Pa12<gpio::PfD>>, hal::sercom::Sercom4Pad2<gpio::Pb10<gpio::PfD>>, hal::sercom::Sercom4Pad3<gpio::Pb11<gpio::PfD>>>;
type CS     = hal::gpio::Pin<PA06, hal::gpio::v2::Output<hal::gpio::v2::PushPull>>;
type RESET  = hal::gpio::Pin<PA08, hal::gpio::v2::Output<hal::gpio::v2::PushPull>>;
type UART   = UART0<hal::sercom::Sercom0Pad3<hal::gpio::Pa11<PfC>>, hal::sercom::Sercom0Pad2<hal::gpio::Pa10<PfC>>, (), ()>;
type RedLED = hal::gpio::Pa17<hal::gpio::Output<hal::gpio::OpenDrain>>;


type FUCK = Sx127x<driver_pal::wrapper::Wrapper<Compat<SPI>, feather_m0::sercom::Error, Compat<feather_m0::gpio::Pin<feather_m0::gpio::v2::PA06, feather_m0::gpio::v2::Output<feather_m0::gpio::v2::PushPull>>>, Compat<feather_m0::gpio::Pin<feather_m0::gpio::v2::PA16, feather_m0::gpio::v2::Input<feather_m0::gpio::v2::Floating>>>, Compat<feather_m0::gpio::Pin<PA19, feather_m0::gpio::v2::Input<feather_m0::gpio::v2::Floating>>>, Compat<feather_m0::gpio::Pin<feather_m0::gpio::v2::PA08, feather_m0::gpio::v2::Output<feather_m0::gpio::v2::PushPull>>>, (), Compat<feather_m0::delay::Delay>, Infallible>, feather_m0::sercom::Error, (), Infallible>;
type IRQ = ExtInt9<hal::gpio::Pin<PA09, hal::gpio::v2::Alternate<hal::gpio::v2::A>>>;
type RTC = feather_m0::rtc::Rtc<ClockMode>;

//static BB: BBBuffer<U6> = BBBuffer( ConstBBBuffer::new() );

// Interrupts: https://docs.rs/atsamd21g/0.9.0/atsamd21g/enum.Interrupt.html
#[rtic::app(device = crate::hal::pac, peripherals = true, dispatchers = [ADC])]
mod app
{
    use crate::*;
    use embedded_hal::timer::CountDown;
    use feather_m0::thumbv6m::usb::usb_device::bus::UsbBusAllocator;
    use embedded_hal::blocking::delay::DelayMs;
    use feather_m0::timer_traits::InterruptDrivenTimer;
    use cortex_m::peripheral::NVIC;
    use usbd_serial::{USB_CLASS_CDC, SerialPort};
    use rtic::export::interrupt::disable;
    use cortex_m::interrupt::enable;
    use heapless::Vec;
    use crate::packets::Packets;
    use heapless::spsc::{Producer, Consumer, Queue};
    use radio::{Transmit, Receive, Interrupts};
    use radio_sx127x::prelude::*;
    use radio_sx127x::device::{regs, State};
    use radio::blocking::BlockingTransmit;
    use feather_m0::rtc::ClockMode;
    use feather_m0::clock::{ClockGenId, ClockSource};
    use rtic::pend;
    use rtic::time::duration::Seconds;
    use core::fmt::Write;
    //use dwt_systick_monotonic::DwtSystick;

    #[resources]
    struct Resources
    {
        //lora:LORA,
        lora:FUCK,
        irq: ExtInt9<hal::gpio::Pin<PA09, hal::gpio::v2::Alternate<hal::gpio::v2::A>>>,
        uart:UART,
        //usb_device: UsbDevice<'static, UsbBus>,
        //usb_serial: SerialPort<'static, UsbBus>,
        tc4: hal::timer::TimerCounter4,
        red_led: RedLED,
        rtc:feather_m0::rtc::Rtc<ClockMode>,
        p: Producer<'static, Packet, { 32_usize }>,
        c: Consumer<'static, Packet, { 32_usize }>,
        timeout:u8
        //delay: Delay
    }

    //#[monotonic(binds = SysTick, default = true)]
    //type MyMono = DwtSystick<8_000_000>; // 8 MHz

    #[init]
    #[no_panic]
    fn init(cx: init::Context) -> (init::LateResources, init::Monotonics)
    {
        static mut USB_ALLOCATOR: Option<UsbBusAllocator<UsbBus>> = None;
        static mut Q: Queue<Packet, 32_usize> = Queue::new();
        //static mut USB_BUS: Option<UsbDevice<UsbBus>> = None;
        //static mut USB_SERIAL: Option<SerialPort<UsbBus>> = None;

        // RF95     Pinout: https://cdn-learn.adafruit.com/assets/assets/000/046/254/original/feather_Feather_M0_LoRa_v1.2-1.png?1504886587
        // RFM69HCW Pinout: https://cdn-learn.adafruit.com/assets/assets/000/046/205/original/Feather_M0_RFM95_v1.2.pdf?1504806819

        let mut peripherals = cx.device;
        let mut pins        = hal::Pins::new(peripherals.PORT);
        let mut core        = cx.core;

        let mut clocks = GenericClockController::with_internal_32kosc(
            peripherals.GCLK,
            &mut peripherals.PM,
            &mut peripherals.SYSCTRL,
            &mut peripherals.NVMCTRL
        );

        let mut delay   = Delay::new(core.SYST, &mut clocks);
        let     cs      = pins.rfm_cs.into_open_drain_output(&mut pins.port);
        let     reset   = pins.rfm_reset.into_open_drain_output(&mut pins.port);
        //let mut irq: feather_m0::eic::pin::ExtInt9<GPIO>  = pins.rfm_irq.into_ei(&mut pins.port);
        let mut random_pin  = pins.d12.into_floating_input(&mut pins.port);
        let mut random_pin1  = pins.d11.into_floating_input(&mut pins.port);

        let mut red_led = pins.d13.into_open_drain_output(&mut pins.port);

        let spi = hal::spi_master(
            &mut clocks,
            MegaHertz(10),
            peripherals.SERCOM4,
            &mut peripherals.PM,
            pins.sck,
            pins.mosi,
            pins.miso,
            &mut pins.port);

        let mut uart = {
            let mut u = hal::uart(
                &mut clocks,
                9600.hz(),
                //115200.hz(),
                //115200.hz(),
                peripherals.SERCOM0,
                &mut peripherals.PM,
                pins.d0.into(),
                pins.d1.into(),
                &mut pins.port
            );

            u.intenset(|w| {
                w.rxc().set_bit();
            });

            u
        };

        debug_nl(&mut uart, b"Alive");

        // https://learn.adafruit.com/adafruit-rfm69hcw-and-rfm96-rfm95-rfm98-lora-packet-padio-breakouts/rfm9x-test


        /*let mut lora = { radio_sx127x::Sx127x::spi(spi,
                                                   cs,
                                                   irq,
                                                   random_pin,
                                                   reset,
                                                   delay,
                                                   &CONFIG_RADIO).unwrap() };*/


        // Do some trickery to get a static usb_allocator.




        // https://github.com/atsamd-rs/atsamd/blob/2128ca05a74eaa66e3a4014071e7caa12b5000c3/boards/feather_m4/examples/trng.rs

        /* TODO: Temporary fix to make Linux allow the USB again after a freeze.
        use unproven;
        let trng = Trng::new(&mut peripherals.MCLK, peripherals.TRNG);*/



        // https://github.com/atsamd-rs/atsamd/blob/08b313a7b7298d8fedffb01f29f3f4b15115b96c/boards/p1am_100/examples/usb_echo_rtic.rs


        // https://github.com/rtic-rs/cortex-m-rtic/blob/master/examples/periodic.rs
        let gclk0 = clocks.gclk0();

        // https://docs.rs/atsamd21g/0.9.0/atsamd21g/

        let eic_clock = clocks.eic(&gclk0).unwrap();
        let mut eic   = EIC::init(&mut peripherals.PM, eic_clock, peripherals.EIC);

        // https://github.com/atsamd-rs/atsamd/blob/master/boards/feather_m0/examples/clock.rs
        let timer_clock = clocks
            .configure_gclk_divider_and_source(ClockGenId::GCLK3, 32, ClockSource::OSC32K, true)
            .unwrap();

        let rtc_clock = clocks.rtc(&timer_clock).unwrap();
        let rtc = Rtc::clock_mode(peripherals.RTC, rtc_clock.freq(), &mut peripherals.PM);

        // https://github.com/mr-glt/sx127x_lora/issues/6

        //cortex_m::interrupt::disable();

        // RFM69HCW and RFM95 LoRa Feather boards (M0) have the same schematic/pinout.
        // Schematic: https://cdn-learn.adafruit.com/assets/assets/000/032/914/original/feather_schem.png?1465421956

        // https://github.com/mr-glt/sx127x_lora
        //lora.set_tx_power(17,1); //Using PA_BOOST. See your board for correct pin.

        // Page 66: FIFO
        // Page 73: Variable length packets
        // Page 40-41: Continuous mode.
        // Pg 100 has address filtering.
        // Check page 96
        // Page 38: Diagram of Tx
        // The FIFO size is fixed to 64 bytes.
        // https://mavlink.io/en/guide/serialization.html#mavlink2_packet_format

        // https://lora-developers.semtech.com/knowledge-base/faq/P160#

        // The maximum packet length is 280 bytes for a signed message that uses the whole payload.
        // The minimum packet length is 12 bytes for acknowledgment packets without payload.

        // Page 74: Unlimited Packet size

        /*
            Unlimited Packet:
            Preamble (1010...)
            Sync word (Network ID)
            Optional Address byte (Node ID)
            Message data
            Optional 2-bytes CRC checksum (Tx only)
         */

        // 0 -> don't need
        // 1 -> Payload length
        // 2 -> incompatability flags
        // 3 -> Compatibility Flags
        // 4 -> Packet sequence number (u8, 0-255)
        // 5 -> System ID (sender)
        // 6 -> Component ID (sender)
        // 7 -> Message ID (low, middle, high bytes) (u32) (store in 4 u8's?)
        // uint16_t checksum Checksum (low byte, high byte)
        //  uint8_t signature[13]
        // Payload (255?)
        // Continuous mode: each bit transmitted or received is accessed in real time at the DIO2/DATA pin. This mode may be used if adequate external signal processing is available.

        // The msgid (message id) field identifies the specific message encoded in the packet.
        //

        // FIFO is split 0x00/0x80 at start up for Rx and Tx.
        // Use can use the whole FIFO by setting the addr to 0x00 in whatever mode you're in.

        //let mavlink_packet = [];

        // TODO: Sync a timer.

        // 1. Listen for timer, also send timer at whatever rate.
        // 2. Either sync our timer, or wait for them to sync their timer. (Bounds check?)
        // 3. Talk and receive in a coherent manner.
        // 4. If we don't receive
        // 5. Build internal "timer" map/struct.

        // TODO: Just iterate
        // TODO: actually you could just increment or decrement a delay.

        //disable();

        /*let mut lora = {
            let mut l = LoRa::new(spi, cs, reset, FREQUENCY, &mut delay).expect("Failed to communicate with radio module!");

            l
        };*/
        //lora.set_tx_power()

        let (p, c) = Q.split();

        red_led.set_high();
        delay.delay_ms(50_u8);
        red_led.set_low();
        delay.delay_ms(50_u8);
        red_led.set_high();
        delay.delay_ms(50_u8);
        red_led.set_low();

        let mut irq = {
            let mut i = pins.rfm_irq.into_ei(&mut pins.port);
            //if i.is_interrupt() { i.clear_interrupt(); }

            i.sense(&mut eic, Sense::RISE);
            i.enable_interrupt(&mut eic);

            i
        };

        let mut tc4 = {

            let mut t = hal::timer::TimerCounter4::tc4_(&clocks.tc4_tc5(&gclk0).unwrap(), peripherals.TC4, &mut peripherals.PM);

            // 1khz works
            //t.start(feather_m0::time::Nanoseconds(1_000_000_000));
            //t.start(1.khz());
            t.start(1.hz());
            //t.start(1.khz());
            t.enable_interrupt();
            t
        };


        // ======================================
        // https://rtic.rs/dev/book/en/by-example/app.html

        // Note: A higher number means a higher priority in RTIC, which is the opposite from what Cortex-M does in the NVIC peripheral.
        // Explicitly, this means that number 10 has a higher priority than number 9.

        /*
            When several tasks are ready to be executed the one with highest static priority will be executed first.
            Task prioritization can be observed in the following scenario: an interrupt signal arrives during the execution of a low priority task; the signal puts the higher
            priority task in the pending state. The difference in priority results in the higher priority task preempting the lower priority one: the execution of the
            lower priority task is suspended and the higher priority task is executed to completion. Once the higher priority
            task has terminated the lower priority task is resumed.
         */

        // ======================================


        /*USB_ALLOCATOR = Some(hal::usb_allocator(
            peripherals.USB,
            &mut clocks,
            &mut peripherals.PM,
            pins.usb_dm,
            pins.usb_dp,
        ));

        let usb_allocator = USB_ALLOCATOR.as_ref().unwrap();

        let usb_serial = SerialPort::new(&usb_allocator);
        let usb_device = UsbDeviceBuilder::new(&usb_allocator, UsbVidPid(0x2234, 0x272d))
            .manufacturer("FUCK123454")
            .product("FUCK1234")
            .serial_number("FUCKFUCK")
            .device_class(USB_CLASS_CDC)
            .build();

        unsafe {
            core.NVIC.set_priority(interrupt::USB, 1);
            NVIC::unmask(interrupt::USB);
        }*/



        //let mut dcb = cx.core.DCB;
        //let dwt = cx.core.DWT;
        //let systick = cx.core.SYST;

        //let mono = DwtSystick::new(&mut dcb, dwt, systick, 8_000_000);

        //demo_task::spawn_after(Seconds(1_u32));

        let resources = init::LateResources
        {
            // For some reason I have to put the initialization within this struct.
            //lora:LoRa::new(spi, cs, reset, FREQUENCY, &mut delay).expect("Failed to communicate with radio module!"),
            lora:radio_sx127x::Sx127x::spi(spi.compat(), cs.compat(), random_pin1.compat(), random_pin.compat(), reset.compat(), delay.compat(), &CONFIG_RADIO).unwrap(),
            //lora:radio_sx127x::Sx127x::spi(spi.compat(), cs.compat(), irq.compat(), random_pin.compat(), reset.compat(), delay.compat(), &CONFIG_RADIO).unwrap(),
            uart:uart,
            //usb_device:usb_device,
            //usb_serial:usb_serial,
            irq:irq,
            tc4:tc4,
            red_led: red_led,
            c:c,
            p:p,
            rtc:rtc,
            timeout:0
            //delay:delay
        };

        //enable();

        (resources, init::Monotonics())
    }

    /*#[task(binds = SERCOM0, resources=[uart])]
    fn poll_uart(cx: poll_uart::Context)
    {
        let mut uart_ = cx.resources.uart;

        uart_.lock(|uart| {
            /*match uart.read()
            {
                Ok(byte) =>
                {
                    //cx.resources.led.toggle().unwrap();

                    for byte in b"hello"
                    {
                        block!(uart.write(*byte)).unwrap();
                    }

                    //block!(cx.resources.uart.write(byte)).unwrap();
                }
                Err(_) => {}
            };*/
        });
    }*/

    #[task(binds = DAC, resources = [lora, uart, timeout])]
    fn beacon_task(mut cx: beacon_task::Context)
    {
        static mut TICK: u8 = 0;

        let mut lora_    = cx.resources.lora;
        let mut uart_    = cx.resources.uart;
        let mut timeout_ = cx.resources.timeout;

        // Note: Once the boards establish communication they can speak as fast as you want.

        fn send_beacon(lora: &mut FUCK)
        {
            lora.start_transmit(b"B");
        }

        lora_.lock(|lora: &mut FUCK| {
            uart_.lock(|uart: &mut UART| {
                timeout_.lock(|t: &mut u8| {
                    //debug(uart, b"Timeout is: ");

                    /*let mut f: heapless::Vec<u8, 32> = heapless::Vec::new();

                    f.write_fmt(format_args!("Timeout is: {}", *t));
                    debug_nl(uart, &f);*/

                    // If the timeout hasn't been set.
                    if *t == 0
                    {
                        *t += 1;
                    }

                    // Otherwise check it to see if we need to issue a beacon.
                    else
                    {
                        // Don't let it accidentally overflow (probably not needed)
                        if *t + 1 >= u8::MAX
                        {
                            *t = 0;
                            return;
                        }

                        *t += 1;

                        // Return because we haven't tripped the timeout.
                        if *t < 3
                        {
                            return;
                        }
                   }

                //lora.write_reg(regs::LoRa::IRQFLAGSMASK, 0b0000_0000);
                send_beacon(lora);
                while !lora.check_transmit().unwrap() {} // TODO: unwrap()
                debug_nl(uart, b"BEACON SENT");
                lora.start_receive();
                });
            });
        });
    }


    #[no_panic]
    #[task(binds = TC4, resources = [tc4])]
    fn timer_4(mut cx: timer_4::Context)
    {
        static mut TICK: u8    = 0;
        static mut SYSTICK: u8 = 0;

        // This was originally contained the beacon function above ^
        // I may move the beacon stuff back in here at some point.
        let mut tc4_   = cx.resources.tc4;

        tc4_.lock(| tc4: &mut hal::timer::TimerCounter4 | {
            if !tc4.wait().is_ok()
            {
                return;
            }

            pend(interrupt::DAC)
        });
    }

    // This is bound to the interrupt pin.
    #[no_panic]
    #[task(binds = EIC, resources = [irq, lora, red_led, uart, timeout], priority = 3)]
    fn lora_interrupt(mut cx: lora_interrupt::Context)
    {
        // Page 110: IRQ flags.
        // On the Feather M0 the "IRQ" pin is DIO0

        // Check the definitions here.
        // https://github.com/lupyuen/LoRaArduino/blob/master/libraries/HopeRFLib/HopeDuino_LoRa.cpp

        /*
            Two registers are used to control the IRQ in LoRa mode, the register RegIrqFlagsMask which is used to mask the
            interrupts and the register RegIrqFlags which indicates which IRQ has been triggerd.
            In the register RegIrqFlagsMask, setting a bit to ‘1’ will mask the interrupt, meaning this interrupt is disactivated.
            By default all the interrupt are available.
            In the register RegIrqFlags, a ‘1’ indicates a given IRQ has been trigged and then the IRQ must be clear by writing a ‘1’
         */

        let mut irq_       = cx.resources.irq;
        let mut lora_      = cx.resources.lora;
        let mut red_led_   = cx.resources.red_led;
        let mut uart_      = cx.resources.uart;
        let mut timeout_   = cx.resources.timeout;

        /*led.set_high();
        lora.try_delay_ms(10);
        led.set_low();*/

        irq_.lock(|irq| {
            lora_.lock(|lora: &mut FUCK|{
                red_led_.lock(|led| {
                    uart_.lock(|uart: &mut UART| {

                    // Will an interrupt fire and block?
                    // TODO: wire up a DIO interrupt.
                    // Guess we'll check to see if we received first.
                    // I don't know why I can't just do i.contains()
                    // This was taken from "lora_get_interrupts()"

                    led.set_high();
                    lora.try_delay_ms(1);
                    led.set_low();

                    //debug_nl(uart, b"INTERRUPT");
                    //usb_serial.write(b"INTERRUPT\r\n").ok();

                    let (interrupts, reg): (Irq, u8) = match get_lora_interrupts(uart, lora)
                    {
                        Ok((i, r)) => (i, r),
                        Err(()) => { return; }
                    };

                    if interrupts.contains(Irq::RX_DONE)
                    {
                        debug_nl(uart, b"RX_DONE");

                        // Send anything if we need to. For now I'll send a demo message.
                        // Clear the interrupts.

                        let mut info = PacketInfo::default();
                        let mut buff = [0u8; 255];

                        // Don't bother if we have a CRC error.
                        // I noticed while testing that messages are indeed corrputed if
                        // there is a CRC error.
                        if interrupts.contains(Irq::CRC_ERROR)
                        {
                            debug_nl(uart, b"CRC_ERROR");
                            lora.write_reg(regs::LoRa::IRQFLAGS, reg);
                            if irq.is_interrupt() { irq.clear_interrupt(); }
                            return;
                        }

                        match lora.get_received(&mut info, &mut buff)
                        {
                            Ok(n) =>
                            {
                                debug_nl(uart, b"GOT MESSAGE, SENDING REPLY");
                                lora.start_transmit(b"PONG");
                                while !lora.check_transmit().unwrap() {
                                    // TODO: unwrap()
                                    //debug_nl(uart, b".");
                                }

                                // A timeout of 0 indicates the board just started.
                                // Setting this to 1 "resets" the timer.
                                timeout_.lock(|t| { *t = 1; });

                                if irq.is_interrupt() { irq.clear_interrupt(); }
                                lora.write_reg(regs::LoRa::IRQFLAGS, reg);

                                lora.start_receive();
                                return;
                            }

                            Err(e) =>
                            {
                                debug_nl(uart, b"Unable to receive.");
                            }
                        }
                    }

                    if interrupts.contains(Irq::TX_DONE)
                    {
                        // Immediately start receiving.
                        debug_nl(uart, b"TX_DONE");
                        lora.start_receive();
                    }

                    // TODO: RX_TIMEOUT
                    if interrupts.contains(Irq::RX_TIMEOUT)
                    {
                        debug_nl(uart, b"RX_TIMEOUT");
                    }

                    if interrupts.contains(Irq::CAD_DONE)
                    {
                        debug_nl(uart, b"CAD_DONE");
                    }

                    if interrupts.contains(Irq::PREAMBLED_DETECT)
                    {
                        debug_nl(uart, b"PREAMBLED_DETECT");
                    }

                    if interrupts.contains(Irq::SYNC_ADDR_MATCH)
                    {
                        debug_nl(uart, b"SYNC_ADDR_MATCH");
                    }

                    if interrupts.contains(Irq::VALID_HEADER)
                    {
                        debug_nl(uart, b"VALID_HEADER");
                    }

                    debug_nl(uart, b"INTERRUPT DONE...");

                    lora.write_reg(regs::LoRa::IRQFLAGS, reg);
                    if irq.is_interrupt() { irq.clear_interrupt(); }

                    });
                });
            });
        });
    }

    /*#[task(binds = USB, resources=[usb_device, usb_serial])]
    fn poll_usb(cx: poll_usb::Context)
    {
        static mut TICK: u64 = 0;

        let mut usb_device_ = cx.resources.usb_device;
        let mut usb_serial_ = cx.resources.usb_serial;

        let mut buf = [0_u8; 64];

        //let usb_device: UsbDevice<'static, UsbBus>  = cx.resources.usb_device;
        //let usb_serial: SerialPort<'static, UsbBus> = cx.resources.usb_serial;

        //usb_device.poll(&mut [usb_serial]);

        usb_device_.lock(|usb_device: &mut UsbDevice<'static, UsbBus>| {
            usb_serial_.lock(|usb_serial: &mut SerialPort<'static, UsbBus>,| {
                usb_device.poll(&mut [usb_serial]);

                if (*TICK + 1_u64) == 500_u64
                {
                    usb_serial.write(b"alive\r").ok();
                    *TICK = 0_u64;
                }

                else
                {
                    *TICK += 1_u64;
                }

                /*match usb_serial.read(&mut buf)
                {
                    Ok(n) =>
                    {
                        if let Some(command) = buf.get(0)
                        {
                            /*if command == "t".as_bytes().first().unwrap()
                            {
                                usb_serial.write(b"alive").ok();
                            }*/

                        }

                        usb_serial.write(b"alive").ok();
                    }

                    Err(e) =>
                    {
                        usb_serial.write(b"FUCK").ok();
                    }
                }*/

                //usb_serial.write(b"test\r\n").ok();

            /*if let Ok(count) = usb_serial.read(&mut buf)
            {
                for (i, c) in buf.iter().enumerate() {
                   if i >= count {
                       break;
                   }
                   usb_serial.write(&[c.clone()]).ok();
                }
            };*/
            });
        });
    }*/


    //#[idle(resources=[c])]
    #[idle]
    fn idle(mut cx: idle::Context) -> !
    {
        //let mut c_ = cx.resources.c;

        loop
        {

            /*if let Some(packet) = c_.lock(|c| c.dequeue())
            {
                //hprintln!("received message: {}", byte).unwrap();
            }*/
            cortex_m::asm::nop()
        }
    }
}


fn debug_nl(uart: &mut UART, msg: &[u8])
{
    for byte in msg
    {
        block!(uart.write(*byte)).ok();
    }

    for byte in b"\n"
    {
        block!(uart.write(*byte)).ok();
    }
}

fn debug(uart: &mut UART, msg: &[u8])
{
    for byte in msg
    {
        block!(uart.write(*byte)).ok();
    }
}



fn debug_lora(uart: &mut UART, lora: &mut FUCK)
{
    use radio::State as _;

    match lora.get_state()
    {
        Ok(s) =>
        {
            match s
            {
                State::Sleep => {
                    debug_nl(uart, b"State: Sleep");
                }
                State::Standby => {
                    debug_nl(uart, b"State: Standby");
                }
                State::FsTx => {
                    debug_nl(uart, b"State: FsTx");
                }
                State::Tx => {
                    debug_nl(uart, b"State: Tx");
                }
                State::Rx => {
                    debug_nl(uart, b"State: Rx");
                }
                _ =>
                {
                    debug_nl(uart, b"State: Pee pee");
                }
            }
        }

        Err(e) =>
        {
            debug_nl(uart, b"Unable to get lora state.");
        }
    }
}

fn get_lora_interrupts(uart: &mut UART, lora: &mut FUCK) -> Result<(Irq, u8), ()>
{
    let reg = match lora.read_reg(regs::LoRa::IRQFLAGS)
    {
        Ok(reg) => reg,

        Err(e) =>
        {
            //usb_serial.write(b"ERROR READING INTERRUPTS\r\n").ok();
            debug_nl(uart, b"Unable to get reg");
            return Err(());
        }
    };

    let interrupts = match Irq::from_bits(reg)
    {
        Some(interrupts) => interrupts,
        None =>
        {
            debug_nl(uart, b"Unable to get interrupts");
            return Err(());
        }
    };

    if interrupts.is_empty()
    {
        debug_nl(uart, b"Interrupts is empty");
        return Err(());
    }

    Ok((interrupts, reg))
}





#[derive(Clone)]
pub struct Node
{
    address: u8,
    last_seen: u64
}

pub trait BasePacket
{
    //fn preamble(self) -> u8;
    fn address(&self) -> u8;
    fn payload(&self) -> &[u8];
}

pub struct Packet
{
    //pub preamble: u8,  // 0-65535 bytes
    //pub sync_word: u8, // 0-8 bytes
    pub address_byte: u8,
    //pub message_data: heapless::Vec<u8, 280>, // The maximum packet length is 280 bytes for a signed message that uses the whole payload.
    //pub message_data: heapless::Vec<u8, 255>, // Temporary for testing.
    pub message_data: heapless::Vec<u8, 255>, // Temporary for testing.
    //pub crc_checksum: Option<u8>
}


impl BasePacket for Packet
{
    fn address(&self) -> u8
    {
        self.address_byte
    }

    fn payload(&self) -> &[u8]
    {
        self.message_data.as_slice()
    }
}

enum ParsePacketError
{
    NoType,
    NoLengthByte,
    NoAddress,
    InvalidType,
    InvalidLength,
    Idk
}

// TODO: queue this up?
// TODO: This will have to be profiled.
fn parse_packet(data: &[u8]) -> Result<Packet, ParsePacketError>
{
    use crate::ParsePacketError::*;

    if data.len() < 3
    {
        return Err(InvalidLength);
    }

    let length_byte = match data.get(0)
    {
        Some(b) => b,
        None => { return Err(NoLengthByte); }
    };

    // Technically optional but I don't want to have to decide if it's apart of the payload or not.
    let address_byte = match data.get(1)
    {
        Some(b) => b,
        None => { return Err(NoAddress); }
    };

    // Custom, not apart of the Semtech spec.
    let packet_type = match data.get(2)
    {
        Some(b) => b,
        None => { return Err(NoType); }
    };


    //let message_data = heapless::Vec::extend_from_slice(&data[3..])
    let message_data: heapless::Vec::<u8, 255> = match data.get(3..)
    {
        Some(b) =>
        {
            match heapless::Vec::from_slice(b)
            {
                Ok(md) => md,
                Err(e) => { return Err(Idk); }
            }
        }

        None =>
        {
            heapless::Vec::new()
        }
    };

    // TODO: just use u8?
    Ok(Packet {
        address_byte: *address_byte,
        message_data: message_data
        //crc_checksum: None
    })

    /*match data.get(0)
    {
        None => { return Err(NoType); }
        Ok(id) =>
        {
            match id
            {
                2 => {
                    // You could do the queue-off here.
                    Ok(Ping)
                }

                _ =>
                {
                    Err(InvalidType)
                }
            }
        },
    }*/
}

/*
    Page: 74+
    FIFO can be prefilled in Sleep/Standby but must be refilled “on-the-fly” during Tx with the rest of the payload.
    1) Pre-fill FIFO (in Sleep/Standby first or directly in Tx mode) until FifoThreshold or FifoFull is set
    2) In Tx, wait for FifoThreshold or FifoEmpty to be set (i.e. FIFO is nearly empty)
    3) Write bytes into the FIFO until FifoThreshold or FifoFull is set.
    4) Continue to step 2 until the entire message has been written to the FIFO (PacketSent will fire when the last bit of the packet has been sent).
 */


/*pub fn mavlink_heartbeat_message() -> mavlink::common::MavMessage {
    mavlink::common::MavMessage::HEARTBEAT(mavlink::common::HEARTBEAT_DATA {
        custom_mode: 0,
        mavtype: mavlink::common::MavType::MAV_TYPE_SUBMARINE,
        autopilot: mavlink::common::MavAutopilot::MAV_AUTOPILOT_ARDUPILOTMEGA,
        base_mode: mavlink::common::MavModeFlag::empty(),
        system_status: mavlink::common::MavState::MAV_STATE_STANDBY,
        mavlink_version: 0x3,
    })
}*/
